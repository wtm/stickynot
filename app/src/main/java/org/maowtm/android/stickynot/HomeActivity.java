package org.maowtm.android.stickynot;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class HomeActivity extends AppCompatActivity {
    protected int nMid = 0;
    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.homelayout);
        TextView addbtn = (TextView) this.findViewById(R.id.addbtn);
        if (addbtn == null) {
            throw new RuntimeException();
        }
        if (savedInstanceState != null) {
            this.nMid = savedInstanceState.getInt("nMid");
        } else {
            NotificationManager nm = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
            nm.cancelAll();
        }
        addbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            HomeActivity.this.addNotification();
            }
        });
    }
    @Override
    protected void onPause () {
        super.onPause();
    }
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt("nMid", nMid);
    }
    protected void addNotification () {
        NotifiItemFragment nlf = new NotifiItemFragment();
        Bundle bArgs = new Bundle();
        bArgs.putInt("mId", nMid ++);
        nlf.setArguments(bArgs);
        this.getSupportFragmentManager().beginTransaction()
                .add(R.id.list, nlf).commit();
    }
}

package org.maowtm.android.stickynot;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class NotifiItemFragment extends Fragment {
    protected NotificationCompat.Builder mBuilder;
    protected Intent resultIntent;
    protected PendingIntent resultPendingIntent;
    protected NotificationManager mNotificationManager;
    protected int mId;
    protected EditText titleEdit;
    protected EditText contentEdit;
    protected TextWatcher watcher;
    protected TextView closeBtn;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            this.mId = savedInstanceState.getInt("mId");
        } else {
            this.mId = this.getArguments().getInt("mId");
        }
        View rootView = inflater.inflate(R.layout.notifiitemlayout, container, false);
        titleEdit = (EditText)rootView.findViewById(R.id.noti_title);
        contentEdit = (EditText)rootView.findViewById(R.id.noti_content);
        closeBtn = (TextView)rootView.findViewById(R.id.close);
        mBuilder = new NotificationCompat.Builder(this.getContext());
        mBuilder.setSmallIcon(R.drawable.notification);
        resultIntent = new Intent(this.getContext().getApplicationContext(), HomeActivity.class);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        resultPendingIntent = PendingIntent.getActivity(this.getContext(), 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        mBuilder.setOngoing(true);
        mNotificationManager = (NotificationManager) this.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                NotifiItemFragment.this.updateNotification();
            }
        };
        updateNotification();
        titleEdit.addTextChangedListener(watcher);
        contentEdit.addTextChangedListener(watcher);
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NotifiItemFragment.this.remove();
            }
        });
        return rootView;
    }
    @Override
    public void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        state.putInt("mId", this.mId);
    }
    public void remove() {
        mNotificationManager.cancel(mId);
        this.getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }
    public void updateNotification() {
        mNotificationManager.cancel(mId);
        String titleText = titleEdit.getText().toString();
        if (titleText.length() == 0)
            titleText = this.getContext().getString(R.string.noti_titlebox_hint);
        mBuilder.setContentTitle(titleText);
        String contentText = contentEdit.getText().toString();
        if (contentText.length() == 0)
            return;
        mBuilder.setContentText(contentText);
        mNotificationManager.notify(mId, mBuilder.build());
    }
    public void onDestroyView() {
        super.onDestroyView();
    }
}
